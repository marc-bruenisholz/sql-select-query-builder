# sql-select-query-builder

Builds an SQL query string from an object model built using classes from *sql-select-models*.

This repository is part of the project SQLQueriesByPropertyPaths.