<?php
/** @noinspection SqlConstantCondition */

/** @noinspection SqlResolve */

/** @noinspection SqlNoDataSourceInspection */

/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlSelectQueryBuilder;

use ch\_4thewin\SqlSelectModels\Arguments\IntArgument;
use ch\_4thewin\SqlSelectModels\Arguments\StringArgument;
use ch\_4thewin\SqlSelectModels\Join;
use ch\_4thewin\SqlSelectModels\Page;
use ch\_4thewin\SqlSelectModels\StringInterface;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\ColumnExpression;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_EQ;
use ch\_4thewin\SqlSelectModels\Ordering;
use ch\_4thewin\SqlSelectModels\Select;
use ch\_4thewin\SqlSelectModels\Table;
use PHPUnit\Framework\TestCase;

class SqlSelectQueryBuilderTest extends TestCase
{
    public function testGroupBy() {
        $personTable = new Table('person', 'id', 'string');
        $selectQueryBuilder = new SqlSelectQueryBuilder((new Select($personTable))
            ->selectColumn(
                new ColumnExpression($personTable, 'FK', 'string')
            )
            ->selectColumn(
                new ColumnExpression($personTable, 'FK', 'string', 'count(${columnReference})')
            )
            ->setGroupByColumnExpressions([new ColumnExpression($personTable, 'FK', 'string')])
        );
        $sqlParameterizedSelectQuery = $selectQueryBuilder->build();
        self::assertEquals(
            'SELECT `person`.`FK`,count(`person`.`FK`) FROM `person` GROUP BY `person`.`FK`',
            $sqlParameterizedSelectQuery->getQueryString()
        );
    }
    public function testCTEs() {
        $personTable = new Table('person', 'id', 'string');
        $cteTable = new Table('cteTable', 'id', 'string');

        $selectQueryBuilder = new SqlSelectQueryBuilder((new Select($cteTable))
            ->addCTE('cteTable', (new Select($personTable))
                ->selectColumn(
                    new ColumnExpression($personTable, 'name', 'string')
                )
            )
            ->addCTE('cteTable2', (new Select($personTable))
                ->selectColumn(
                    new ColumnExpression($personTable, 'name', 'string')
                )
            )
            ->selectColumn(
                new ColumnExpression($cteTable, 'name', 'string')
            ));


        $sqlParameterizedSelectQuery = $selectQueryBuilder->build();
        self::assertEquals(
            'WITH cteTable AS (SELECT `person`.`name` FROM `person`),cteTable2 AS (SELECT `person`.`name` FROM `person`) SELECT `cteTable`.`name` FROM `cteTable`',
            $sqlParameterizedSelectQuery->getQueryString()
        );
    }

    public function testAggregations() {
        $personTable = new Table('person', 'id', 'string','p');

        $selectQueryBuilder = new SqlSelectQueryBuilder(
            (new Select($personTable))
                ->selectColumn(
                    new ColumnExpression($personTable, 'salary',  'string', 'sum(${columnReference})')
                )
                ->selectColumn(
                    new ColumnExpression($personTable, 'salary', 'string', 'avg(${columnReference})')
                )
        );
        $sqlParameterizedSelectQuery = $selectQueryBuilder->build();
        self::assertEquals(
            'SELECT sum(`p`.`salary`),avg(`p`.`salary`) FROM `person` `p`',
            $sqlParameterizedSelectQuery->getQueryString()
        );
    }

    public function test()
    {
        $personTable = new Table('person', 'id', 'string','p');
        $addressTable = new Table('address', 'id', 'string','a');
        $whereCondition = new _EQ(
            new ColumnExpression($personTable, 'salary', 'string'),
            new IntArgument(3)
        );

        $selectQueryBuilder = new SqlSelectQueryBuilder(
            (new Select($personTable))
                ->setIsDistinct(true)
                ->addJoin(
                    new Join(
                        $personTable,
                        'a_id',
                        $addressTable,
                        false,
                        new _EQ(
                            new ColumnExpression($addressTable, 'street', 'string'),
                            new StringArgument('Sesamstreet')
                        ), 'INNER'
                    )
                )
                ->setWhereCondition($whereCondition)
                ->setPage(new Page(100, 0))
        );

        $sqlParameterizedSelectQuery = $selectQueryBuilder->build();
        self::assertEquals(
            'SELECT DISTINCT * FROM `person` `p` JOIN `address` `a` ON `a`.`id` = `p`.`a_id` AND `a`.`street` = ? WHERE `p`.`salary` = ? LIMIT 100 OFFSET 0',
            $sqlParameterizedSelectQuery->getQueryString()
        );
        self::assertEquals(new StringArgument('Sesamstreet'), $sqlParameterizedSelectQuery->getArguments()[0]);
        self::assertEquals(new IntArgument(3), $sqlParameterizedSelectQuery->getArguments()[1]);


        $selectQueryBuilder->getSelect()
            ->selectColumn(new ColumnExpression($personTable, 'firstName', 'string'))
            ->selectColumn(
                new ColumnExpression($personTable, 'lastName', 'string'),
                'last name'
            )
            ->addOrdering(new Ordering(new class implements StringInterface{
                function toString(): string
                {
                    return '`first name`';
                }
            }))
            ->addOrdering(new Ordering(new class implements StringInterface{
                function toString(): string
                {
                    return '`last name`';
                }
            }, false));

        $sqlParameterizedSelectQuery = $selectQueryBuilder->build();

        self::assertEquals(
            'SELECT DISTINCT `p`.`firstName`,`p`.`lastName` `last name` FROM `person` `p` JOIN `address` `a` ON `a`.`id` = `p`.`a_id` AND `a`.`street` = ? WHERE `p`.`salary` = ? ORDER BY `first name`,`last name` DESC LIMIT 100 OFFSET 0',
            $sqlParameterizedSelectQuery->getQueryString()
        );


        $selectQueryBuilder->setSelect((new Select(new Table('dummyTable', 'id', 'string')))->setPage(new Page(10, 0)));
        $sqlParameterizedSelectQuery = $selectQueryBuilder->build();

        self::assertEquals('SELECT * FROM `dummyTable` LIMIT 10 OFFSET 0', $sqlParameterizedSelectQuery->getQueryString());


        $selectQueryBuilder->getSelect()->setWhereCondition($whereCondition);
        $selectQueryBuilder->getSelect()->addJoin(
            new Join(new Table('dummyTable', 'id', 'string'), 'a_id', new Table('address', 'id', 'string','a'), false)
        );
        $sqlParameterizedSelectQuery = $selectQueryBuilder->build();

        self::assertEquals(
            'SELECT * FROM `dummyTable` LEFT JOIN `address` `a` ON `a`.`id` = `dummyTable`.`a_id` WHERE `p`.`salary` = ? LIMIT 10 OFFSET 0',
            $sqlParameterizedSelectQuery->getQueryString()
        );
        self::assertEquals([new IntArgument(3)], $sqlParameterizedSelectQuery->getArguments());
    }

}
