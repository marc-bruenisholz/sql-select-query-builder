<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlSelectQueryBuilder;

use ch\_4thewin\SqlSelectModels\Select;
use ch\_4thewin\SqlSelectModels\Table;
use ch\_4thewin\SqppDbConnectorInterface\SqlParameterizedSelectQuery;
use RuntimeException;

/**
 * Builds a sql select query string.
 * @package ch\_4thewin\SqlSelectQueryBuilder
 */
class SqlSelectQueryBuilder
{
    /** @var Select  */
    protected Select $select;

    /**
     * @param Select $select
     */
    public function __construct(Select $select)
    {
        $this->select = $select;
    }

    /**
     * @return SqlParameterizedSelectQuery
     */
    public function build(): SqlParameterizedSelectQuery
    {
        $arguments = [];
        $query = '';
        if(count($this->select->getAllCTEs()) > 0) {
           $query .= 'WITH ';
           $CTEs = [];
           foreach($this->select->getAllCTEs() as $cteName => $cteSelect) {
               // TODO consider if it would be better if the builder were not stateful (without $select member variable)
               //   and could be reused with different select.
               $builtCteSelect =  (new SqlSelectQueryBuilder($cteSelect))->build();
               $cteQueryString = $builtCteSelect->getQueryString();
               $cteArguments = $builtCteSelect->getArguments();
               foreach($cteArguments as $cteArgument) {
                   $arguments[] = $cteArgument;
               }
               $CTEs[] = $cteName . ' AS (' . $cteQueryString . ')';
           }
           $query .= implode(',', $CTEs) . ' ';
        }
        $query .= 'SELECT ';
        if ($this->select->isDistinct()) {
            $query .= 'DISTINCT ';
        }
        if (count($this->select->getSelectedColumns()) === 0) {
            $query .= '*';
        } else {
            $first = true;
            foreach ($this->select->getSelectedColumns() as $column) {
                if ($first) {
                    $first = false;
                } else {
                    $query .= ',';
                }
                $query .= $column->getColumnExpression()->toString();
                if ($column->getAlias() !== null) {
                    $query .= " `{$column->getAlias()}`";
                }
            }
        }
        $fromClause = $this->select->getFromClause();
        if($fromClause instanceof Select) {
            $builtSubSelect = (new SqlSelectQueryBuilder($fromClause))->build();
            $subSelect = $builtSubSelect->getQueryString();
            $subSelectArguments = $builtSubSelect->getArguments();
            foreach($subSelectArguments as $subSelectArgument) {
                $arguments[] = $subSelectArgument;
            }
            $query .= ' FROM (' . $subSelect . ')';
            $query .= " `{$fromClause->getAlias()}`";
        } elseif($fromClause instanceof Table) {
            $query .= " FROM `{$fromClause->getName()}`";
            if ($fromClause->getAlias() !== $fromClause->getName()) {
                $query .= " `{$fromClause->getAlias()}`";
            }
        }

        foreach ($this->select->getJoins() as $join) {
            $owningTable = $join->getOwningTable();
            $owningTableName = $owningTable->getAlias() ?? $owningTable->getName();
            $foreignKeyColumnName = $join->getForeignKeyColumnName();
            $inverseTable = $join->getInverseTable();
            $inverseTableName = $inverseTable->getAlias() ?? $inverseTable->getName();
            $primaryKeyColumnName = $inverseTable->getPrimaryKeyColumnName();

            // The JOIN type can be set explicitly, or it depends on the search condition.
            $explicitType = $join->getType();
            if($explicitType !== null) {
                if($join->getType() === 'LEFT' || $join->getType() === 'INNER') {
                    $joinType = $join->getType() === 'LEFT' ? 'LEFT ' : '' ;
                } else {
                    throw new RuntimeException('Unknown Join Type ' . $explicitType);
                }
            } else {
                $joinType = 'LEFT ';
            }

            $tableToJoin = $join->isFromInverseToOwningTable() ? $join->getOwningTable() : $join->getInverseTable();
            $joinTableAliasExtension = '';
            if ($tableToJoin->getAlias() !== $tableToJoin->getName()) {
                $joinTableAliasExtension .= " `{$tableToJoin->getAlias()}`";
            }

            $conditionString = "`$inverseTableName`.`$primaryKeyColumnName` = `$owningTableName`.`$foreignKeyColumnName`";

            if ($join->getCondition() !== null) {
                $parameterizedCondition = $join->getCondition();
                $additionalConditionString = $parameterizedCondition->toString();
                foreach($parameterizedCondition->getArguments() as $argument) {
                    $arguments[] = $argument;
                }
                if($additionalConditionString !== '') {
                    $conditionString .= ' AND ' . $additionalConditionString;
                }
            }

            $query .= " {$joinType}JOIN `{$tableToJoin->getName()}`$joinTableAliasExtension ON $conditionString";
        }
        if ($this->select->getWhereCondition() !== null) {
            foreach($this->select->getWhereCondition()->getArguments() as $argument) {
                $arguments[] = $argument;
            }
            $whereConditionString = $this->select->getWhereCondition()->toString();
            if($whereConditionString !== '') {
                $query .= " WHERE " . $whereConditionString;
            }
        }
        if(count($this->select->getGroupByColumnExpressions()) > 0) {
            $query .= ' GROUP BY ';
            $groupByColumnExpressionStrings = [];
            foreach($this->select->getGroupByColumnExpressions() as $expression) {
                $groupByColumnExpressionStrings[] = $expression->toString();
            }
            $query .= implode(',', $groupByColumnExpressionStrings);
        }
        if (count($this->select->getOrderings()) > 0) {
            $query .= ' ORDER BY ';
            $first = true;
            foreach ($this->select->getOrderings() as $ordering) {
                if ($first) {
                    $first = false;
                } else {
                    $query .= ',';
                }
                $query .= $ordering->getColumnExpression()->toString() . ($ordering->isAscending() ? '' : ' DESC');
            }
        }

        // LIMIT
        $page = $this->select->getPage();
        if($page != null) {
            $query .= ' LIMIT ' . $page->getLimit() . ' OFFSET ' . $page->getOffset();
        }

        return new SqlParameterizedSelectQuery($query, $arguments);
    }

    /**
     * @return Select
     */
    public function getSelect(): Select
    {
        return $this->select;
    }

    public function setSelect(Select $select): self
    {
        $this->select = $select;
        return $this;
    }
}